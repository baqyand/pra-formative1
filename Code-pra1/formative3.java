import java.util.*;

/**
*code di bawah merupakan contoh code penggunaan arraylist
*keduanya memiliki kesamaan pada inisialisasi array list 
*di class task 31 menggunakan iterator yang artinya menginisialisasi method mterator yang merdapat pada object list
*kemudian menggunakan perulangan untuk mengecek iterator apakah mempunyai elemen selanjutnya atau tidak
*Jika Iya/True Maka Iterator Akan Mengambil Nilai Selanjutnya
*dan outputnya akan ke bawah (tidak berbentuk []/array )
*untuk yang task 32 itu outputnya adalah [Mango, Apple, Banana, Grapes] 

    */



public class Task31 {
  public static void main(String args[]) {
    ArrayList<String> list = new ArrayList<String>();
    list.add("Mango");
    list.add("Apple");
    list.add("Banana");
    list.add("Grapes");
    Iterator itr = list.iterator();
    while (itr.hasNext()) {
      System.out.println(itr.next());
    }
    System.out.println(list);
  }
}

public class Task32 {
  public static void main(String args[]) {
    ArrayList<String> list = new ArrayList<String>();
    list.add("Mango");
    list.add("Apple");
    list.add("Banana");
    list.add("Grapes");
    System.out.println(list);
  }
}