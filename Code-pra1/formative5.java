import java.util.*;

/**
*jadi ini adalah contoh dari hashmap program
*namun ternyata oh ternyata programnya error
*karena di value ke 5 itu isi datanya pisang,pisang sehingga program akan error
*oleh karena itu saya mencoba mengganti value pertama menjadi 5
*sehingga program berjalan dengan sempurna

*penjelasan programnya
*jadi inisialissi object terlebih dahulu dengan hashmap, dan tipedata nya adalah int dan string
*lalu mengisi datanya dengan menggunakan put(int, String)
*kemudian melakukan iterasi dengan cara foreach looping
dengan looping mengambil value entri setnya
*dan memberikan keluaran getkey dan get value
*sehingga hasilnya akan seperti ini
1 Mango
2 Apple
3 Banana
4 Grapes
5 Pisang

*/

public class HashMapExample1{
 public static void main(String args[]){
   HashMap<Integer,String> map=new HashMap<Integer,String>();
   map.put(1,"Mango");
   map.put(2,"Apple");
   map.put(3,"Banana");
   map.put(4,"Grapes");
   map.put(5,"Pisang");

   System.out.println("Iterating Hashmap...");
   for(Map.Entry m : map.entrySet()){
    System.out.println(m.getKey()+" "+m.getValue());
   }
  }
}
