public class Task2 {
  public static void main(String[] args) {

// ini merupakan contoh penggunaan if else condition

    int age = 25;//inisialisasi data
    int weight = 48;//inisialisasi data

//di sini di jelaskan bahwa jika value age lebih besar dari 18 maka akan lanjut ke if berikutnya
// ini merupakan nested if code
    if (age >= 18) {

//lalu jika weight lebih tinggi dari 50 maka akan ada output You are eligible to donate blood
      if (weight > 50) {
        System.out.println("You are eligible to donate blood");
//ternyata nilai weight tidak lebih besar dari 50 
      } else {
//sehingga outputnya adalah You are not eligible to donate blood
        System.out.println("You are not eligible to donate blood");
      }
    } else {
      System.out.println("Age must be greater than 18");
    }
  }
}