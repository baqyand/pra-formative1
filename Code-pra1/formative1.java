public class Task1 {
  public static void main(String[] args) {
    int i = 1;//berikut adalah inisialisai tipe data

//berikut adalah perulangan do while
// di mana akan ada break jika value i == 5
    do {
//di sini jika i == 5 maka program akan break
      if (i == 5) {

        i++;
        break;
      }
      System.out.println(i);
//increment untuk value i di luar if condition
      i++;
// saat program di jalan kan maka value yang keluar hanya akan ada 4 karena ada break di if condition
//sehingga outputnya adalah
//1
//2
//3
//4
    } while (i <= 10);
  }
}